# Node APIs

## Async hooks

The async_hooks module provides an API to track asynchronous resources. It can be accessed using:

    const async_hooks = require('async_hooks');

### Terminology

An asynchronous resource represents an object with a callback. This callback can be called multiple times. 

### Overview

      const async_hooks = require('async_hooks');

      // Return the ID of the current execution context.
      const eid = async_hooks.executionAsyncId();

      // Return the ID of the handle responsible for triggering the callback of the current execution scope to call.
      const tid = async_hooks.triggerAsyncId();

      // Create a new AsyncHook instance. All of these callbacks are optional.
      const asyncHook = async_hooks.createHook({ init, before, after, destroy, promiseResolve });

      // Allow callbacks of this AsyncHook instance to call. This is not an implicit action after running the constructor, and must be explicitly run to begin executing callbacks.
      asyncHook.enable();

      // Disable listening for new asynchronous events.
      asyncHook.disable();

      // The following are the callbacks that can be passed to createHook().

      // init is called during object construction. The resource may not have completed construction when this callback runs, therefore all fields of the resource referenced by "asyncId" may not have been populated.
      function init(asyncId, type, triggerAsyncId, resource) { }

      // Before is called just before the resource's callback is called. It can be called 0-N times for handles (e.g. TCPWrap), and will be called exactly 1 time for requests (e.g. FSReqCallback).
      function before(asyncId) { }

      // After is called just after the resource's callback has finished.
      function after(asyncId) { }

      // Destroy is called when the resource is destroyed.
      function destroy(asyncId) { }

      // promiseResolve is called only for promise resources, when the `resolve` function passed to the `Promise` constructor is invoked (either directly or through other means of resolving a promise).
      function promiseResolve(asyncId) { }

### Callbacks

1. init <Function> The init callback.
2. before <Function> The before callback.
3. after <Function> The after callback.
4. destroy <Function> The destroy callback.
5. promiseResolve <Function> The promiseResolve callback.<br>

All the callbacks are optional. Only the callback needed for the specific task needs to be passed.

### Error handling

If any AsyncHook callback throw an error, the application will print the stack track and exit. The exit callbacks will still be called unless the application is run with --abort-on-uncaught-exception.

### Printing in AsyncHooks callbacks

Using asynchronous operations inside an AsyncHooks callback function will cause an infinite recursion. Because asynchronous operations will cause the AsyncHooks callbacks to bo called.

## Errors

There are generally four types of errors in Node.js.<br>
1. Standard JavaScript errors such as EvalError, SyntaxError, RangeError, ReferenceError, TypeError, and URIError.
2. System errors.
3. User-specified errors.
4. AssertionErrors.

### Error propagation and interception
How the errors are reported and handled depends entirely on the type of error and the style of API that is called.<br>
* Most asynchronous methods that accept a callback function will accept an Error object passed as the first argument to that function. If error occurred than it should be handled.

        const fs = require('fs');
        fs.readFile('a file that does not exist', (err, data) => {
        if (err) {
        console.error('There was an error reading the file!', err);
        return;
        }
        // Otherwise handle the data
        });
    
* When an asynchronous method is called on an object that is an EventEmitter, errors can be routed to that object's 'error' event.

        const net = require('net');
        const connection = net.connect('localhost');
        // Adding an 'error' event handler to a stream:
        connection.on('error', (err) => {
        // If the connection is reset by the server, or if it can't
        // connect at all, or on any sort of error encountered by
        // the connection, the error will be sent here.
        console.error(err);
        });
        connection.pipe(process.stdout);

### Handling exceptions

An exception handler is a try/catch statement.<br>
Any exception raised in the lines of code included in the try block is handled in the corresponding catch block:

    try {
      //lines of code
    } catch (e) {}

e in this example is the exception value.

## Resources

* [nodejs.org](https://nodejs.org/docs/latest/api/) documentation.
* [nodejs.dev](https://nodejs.dev/learn/error-handling-in-nodejs) documentation.